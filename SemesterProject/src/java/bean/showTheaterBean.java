/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import ejb.TheaterEJB;
import entity.Movie;
import entity.MovieTimes;
import entity.Theater;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import javax.faces.context.FacesContext;

/**
 *
 * @author carmen
 */
@Named(value = "showTheaterBean")
@SessionScoped
public class showTheaterBean implements Serializable {

    @EJB
    private TheaterEJB theaterEJB;
    private String zipCode;
    private String theaterId;
    private Theater theater;
    private List<Movie> movieList;
    private Movie movie;
    private List<MovieTimes> movieTimesList;
    private String movieTime;
    private int numberOfTickets;

    /**
     * Creates a new instance of showTheaterBean
     */
    public showTheaterBean() {
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Theater getTheater() {
        return theater;
    }

    public void setTheater(Theater theater) {
        this.theater = theater;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }

    public List<MovieTimes> getMovieTimesList() {
        return movieTimesList;
    }

    public void setMovieTimesList(List<MovieTimes> movieTimesList) {
        this.movieTimesList = movieTimesList;
    }

    public String getMovieTime() {
        return movieTime;
    }

    public void setMovieTime(String movieTime) {
        this.movieTime = movieTime;
    }

    public int getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(int numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public String goToTheaterListPage() {
        return "theaterList.xhtml";
    }

    public List<Theater> getTheaterList() {
        return theaterEJB.getTheaterByZip(Integer.parseInt(zipCode));
    }

    public String showMovies() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        theaterId = params.get("theaterId");
        theater = theaterEJB.saveTheater(theaterId);
        movieList = theaterEJB.getMovies(theaterId);
        return "movieList.xhtml";
    }

    public void showMovieTimes(String movieId) {
        movieTimesList = theaterEJB.getMovieTimes(theaterId, movieId);
    }

    public String purchaseMovieTime(String movieId) {
        movie = theaterEJB.saveMovie(movieId);
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        movieTime = params.get("movieTime");
        return "selectNumberOfTickets.xhtml";
    }

    public int calculateCostOfMovieTickets() {
        return numberOfTickets * 10;
    }
}
