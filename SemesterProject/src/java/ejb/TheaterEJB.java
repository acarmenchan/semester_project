/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.Movie;
import entity.MovieTimes;
import entity.Theater;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author carmen
 */
@Stateless
public class TheaterEJB {

    @PersistenceContext(unitName = "SemesterProjectPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public List<Theater> getTheaterByZip(int zipCode) {
        return em.createNamedQuery("Theater.findByZip", Theater.class)
                .setParameter("zip", zipCode)
                .getResultList();
    }

    public Theater saveTheater(String theaterId) {
        return em.createNamedQuery("Theater.findByTheaterId", Theater.class)
                .setParameter("theaterId", theaterId)
                .getSingleResult();
    }

    public List<Movie> getMovies(String theaterId) {
        return em.createNamedQuery("Theater.findMovies", Movie.class)
                .setParameter("theaterId", theaterId)
                .getResultList();
    }

    public Movie saveMovie(String movieId) {
        return em.createNamedQuery("Movie.findByMovieId", Movie.class)
                .setParameter("movieId", movieId)
                .getSingleResult();
    }

    public List<MovieTimes> getMovieTimes(String theaterId, String movieId) {
        return em.createNamedQuery("MovieTimes.fineByTheaterIdAndMovieId", MovieTimes.class)
                .setParameter("theaterId", theaterId)
                .setParameter("movieId", movieId)
                .getResultList();
    }

}
