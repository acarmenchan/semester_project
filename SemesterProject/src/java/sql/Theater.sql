/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  carmen
 * Created: Aug 2, 2017
 */

/*
DROP TABLE movie_times;
DROP TABLE theater;
DROP TABLE movie;
*/

CREATE TABLE theater (
    theater_id varchar(5) NOT NULL,
    name varchar(25),
    street varchar(25),
    city varchar(20),
    state varchar(2),
    zip Integer,
    PRIMARY KEY (theater_id)
);

CREATE TABLE movie (
    movie_id varchar(5) NOT NULL,
    title varchar(50),
    rating varchar(5),
    duration varchar(10),
    type varchar(35),
    description varchar(400),
    movie_img varchar(30),
    PRIMARY KEY (movie_id)
);

CREATE TABLE movie_times (
    theater_id varchar(5),
    movie_id varchar(5),
    time1 varchar(10),
    time2 varchar(10),
    time3 varchar(10),
    time4 varchar(10),
    time5 varchar(10),
    PRIMARY KEY (theater_id, movie_id),
    FOREIGN KEY (theater_id) REFERENCES theater(theater_id),
    FOREIGN KEY (movie_id) REFERENCES movie(movie_id)
);

INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t1', 'AMC NorthPark', '8687 N Central Expy #3000', 'Dallas', 'TX', 75252);
INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t2', 'Venetian Cinemas', '2661 Midway Rd #200', 'Dallas', 'TX', 75252);
INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t3', 'The Magnolia Theatre', '3699 McKinney Ave', 'Dallas', 'TX', 75252);
INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t4', 'Alama Drafthouse Cinema', '100 S Central Expy', 'Richardson', 'TX', 75080);
INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t5', 'Inwood Theatre', '5458 W Lovers Ln', 'Richardson', 'TX', 75080);
INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t6', 'Angelika Film Center', '7205 Bishop Rd', 'Richardson', 'TX', 75080);
INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t7', 'Cinemark IMAX Theatre', '13331 Preston Rd #2300', 'Richardson', 'TX', 75080);
INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t8', 'Studio Movie Grill', '13933 N Central Expy', 'Plano', 'TX', 75093);
INSERT INTO theater (theater_id, name, street, city, state, zip) 
    VALUES ('t9', 'Cinemark West Plano', '3800 Dallas Pkwy', 'Plano', 'TX', 75093);


INSERT INTO movie (movie_id, title, rating, duration, type, description, movie_img)
    VALUES ('m1', 'Spider-Man: Homecoming', 'PG-13', '2h 13min', 'Action, Adventure, Sci-Fi', 'Peter Parker, with the help of his mentor Tony Stark, tries to balance his life as an ordinary high school student in New York City while fighting crime as his superhero alter ego Spider-Man when a new threat emerges.', 'images/spiderman.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m2', 'Valerian and the City of a Thousand Planets', 'PG-13', '2h 17min', 'Action, Adventure, Fantasy', 'A dark force threatens Alpha, a vast metropolis and home to species from a thousand planets. Special operatives Valerian and Laureline must race to identify the marauding menace and safeguard not just Alpha, but the future of the universe.', 'images/valerian.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m3', 'Atomic Blonde', 'R', '1h 55min', 'Action, Mystery, Thriller', 'An undercover MI6 agent is sent to Berlin during the Cold War to investigate the murder of a fellow agent and recover a missing list of double agents.', 'images/atomic.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m4', 'Detroit', 'R', '2h 23min', 'Crime, Drama, History', 'Amidst the chaos of the Detroit Rebellion, with the city under curfew and as the Michigan National Guard patrolled the streets, three young African American men were murdered at the Algiers Motel.', 'images/detroit.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m5', 'Dunkirk', 'PG-13', '1h 46min', 'Action, Drama, History', 'Allied soldiers from Belgium, the British Empire and France are surrounded by the German army and evacuated during a fierce battle in World War II.', 'images/dunkirk.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m6', 'Girls Trip', 'R', '2h 2min', 'Comedy', 'When four lifelong friends travel to New Orleans for the annual Essence Festival, sisterhoods are rekindled, wild sides are rediscovered, and there''s enough dancing, drinking, brawling, and romancing to make the Big Easy blush.', 'images/girls.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m7', 'The Big Sick', 'R', '2h', 'Comedy, Romance', 'Pakistan-born comedian Kumail Nanjiani and grad student Emily Gordon fall in love but struggle as their cultures clash. When Emily contracts a mysterious illness, Kumail finds himself forced to face her feisty parents, his family''s expectations, and his true feelings.', 'images/bigsick.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m8', 'The Dark Tower', 'PG-13', '1h 35min', 'Action, Adventure, Fantasy', 'The last Gunslinger, Roland Deschain, has been locked in an eternal battle with Walter O''Dim, also known as the Man in Black, determined to prevent him from toppling the Dark Tower, which holds the universe together. With the fate of the worlds at stake, good and evil will collide in the ultimate battle as only Roland can defend the Tower from the Man in Black.', 'images/dark.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m9', 'War for the Planet of the Apes', 'PG-13', '2h 20min', 'Action, Adventure, Drama', 'After the apes suffer unimaginable losses, Caesar wrestles with his darker instincts and begins his own mythic quest to avenge his kind.', 'images/war.jpg');
INSERT INTO movie (movie_id, title, rating, duration, type,  description, movie_img)
    VALUES ('m10', 'Baby Driver', 'R', '1h 52min', 'Action, Crime, Music', 'After being coerced into working for a crime boss, a young getaway driver finds himself taking part in a heist doomed to fail.', 'images/baby.jpg');


INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t1', 'm1', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t1', 'm2', '9:30a', '12:20p', '3:10p', '6:00p', '8:40p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t1', 'm3', '10:15a', '2:00p', '4:45p', '7:30p', '10:50p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t1', 'm4', '11:00a', '1:45p', '4:15p', '8:10p', '9:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t1', 'm6', '11:25a', '1:30p', '3:00p', '5:45p', '7:00p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t1', 'm10', '10:40a', '2:15p', '4:20p', '6:15p', '8:25p');    
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t1', 'm9', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');


INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t2', 'm8', '11:20a', '1:45p', '4:20p', '6:40p', '9:15p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t2', 'm7', '10:45a', '12:20p', '7:40p', '9:30p', '11:00p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t2', 'm9', '11:15p', '2:20p', '7:40p', '9:30p', '10:50p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t2', 'm5', '9:00a', '12:15p', '3:40p', '7:10p', '9:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t2', 'm6', '9:30a', '1:15p', '3:40p', '7:10p', '10:25p');
 INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t2', 'm1', '10:20p', '1:20p', '7:40p', '9:30p', '10:50p');  

INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t3', 'm6', '11:00a', '1:45p', '4:40p', '7:30p', '10:20p');        
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t3', 'm5', '1:20p', '4:20p', '7:40p', '9:30p', '10:50p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t3', 'm10', '9:00a', '12:15p', '3:40p', '7:10p', '9:25p');        
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t3', 'm4', '11:00a', '1:45p', '4:40p', '7:30p', '10:20p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t3', 'm8', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t3', 'm2', '11:20a', '1:45p', '4:20p', '6:40p', '9:15p');

INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t4', 'm3', '11:15a', '2:10p', '4:45p', '7:30p', '10:15p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t4', 'm1', '10:45a', '12:20p', '7:40p', '9:30p', '11:00p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t4', 'm6', '9:30a', '1:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t4', 'm10', '9:00a', '12:15p', '3:40p', '7:10p', '9:25p');   
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t4', 'm2', '11:20a', '1:45p', '4:20p', '6:40p', '9:15p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t4', 'm9', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t4', 'm5', '1:20p', '4:20p', '7:40p', '9:30p', '10:50p');

INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t5', 'm4', '11:00a', '1:45p', '4:15p', '8:10p', '9:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t5', 'm9', '11:15p', '2:20p', '7:40p', '9:30p', '10:50p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t5', 'm1', '9:30a', '12:20p', '3:10p', '6:00p', '8:40p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t5', 'm7', '10:45a', '12:20p', '7:40p', '9:30p', '11:00p');    

INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t6', 'm7', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t6', 'm2', '10:00a', '12:50p', '3:45p', '6:55p', '10:10p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t6', 'm8', '9:45a', '12:20p', '7:40p', '9:30p', '11:00p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t6', 'm10', '9:00a', '12:15p', '3:40p', '7:10p', '9:25p');        
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t6', 'm4', '11:00a', '1:45p', '4:40p', '7:30p', '10:20p');

INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t7', 'm2', '10:20a', '12:40p', '4:05p', '7:25p', '10:40p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t7', 'm5', '11:20p', '2:20p', '7:40p', '9:30p', '10:50p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t7', 'm6', '9:30a', '1:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t7', 'm1', '10:45a', '12:20p', '7:40p', '9:30p', '11:00p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t7', 'm7', '9:45a', '12:20p', '7:40p', '9:30p', '11:00p'); 
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t7', 'm9', '11:15p', '2:20p', '7:40p', '9:30p', '10:50p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t7', 'm8', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');

INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t8', 'm5', '10:50a', '2:00p', '5:05p', '8:10p', '11:15p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t8', 'm9', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t8', 'm6', '9:30a', '1:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t8', 'm10', '9:00a', '12:15p', '3:40p', '7:10p', '9:25p');   
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t8', 'm2', '11:20a', '1:45p', '4:20p', '6:40p', '9:15p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t8', 'm3', '10:15a', '2:00p', '4:45p', '7:30p', '10:50p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t8', 'm1', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');

INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm3', '11:15a', '2:10p', '4:45p', '7:30p', '10:15p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm1', '10:45a', '12:20p', '7:40p', '9:30p', '11:00p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm6', '9:30a', '1:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm10', '9:00a', '12:15p', '3:40p', '7:10p', '9:25p');   
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm2', '11:20a', '1:45p', '4:20p', '6:40p', '9:15p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm9', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm5', '1:20p', '4:20p', '7:40p', '9:30p', '10:50p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm4', '11:00a', '1:45p', '4:15p', '8:10p', '9:25p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm7', '9:45a', '12:20p', '7:40p', '9:30p', '11:00p');
INSERT INTO movie_times (theater_id, movie_id, time1, time2, time3, time4, time5)
    VALUES ('t9', 'm8', '9:00a', '12:15p', '3:40p', '7:10p', '10:25p');