/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author carmen
 */
@Entity
@Table(name = "MOVIE_TIMES")
@NamedQueries({
    @NamedQuery(name = "MovieTimes.findAll", query = "SELECT m FROM MovieTimes m")
    , @NamedQuery(name = "MovieTimes.findByTheaterId", query = "SELECT m FROM MovieTimes m WHERE m.movieTimesPK.theaterId = :theaterId")
    , @NamedQuery(name = "MovieTimes.findByMovieId", query = "SELECT m FROM MovieTimes m WHERE m.movieTimesPK.movieId = :movieId")
    , @NamedQuery(name = "MovieTimes.fineByTheaterIdAndMovieId", 
            query = "SELECT m from MovieTimes m WHERE m.movieTimesPK.theaterId = :theaterId AND m.movieTimesPK.movieId = :movieId")    
    , @NamedQuery(name = "MovieTimes.findByTime1", query = "SELECT m FROM MovieTimes m WHERE m.time1 = :time1")
    , @NamedQuery(name = "MovieTimes.findByTime2", query = "SELECT m FROM MovieTimes m WHERE m.time2 = :time2")
    , @NamedQuery(name = "MovieTimes.findByTime3", query = "SELECT m FROM MovieTimes m WHERE m.time3 = :time3")
    , @NamedQuery(name = "MovieTimes.findByTime4", query = "SELECT m FROM MovieTimes m WHERE m.time4 = :time4")
    , @NamedQuery(name = "MovieTimes.findByTime5", query = "SELECT m FROM MovieTimes m WHERE m.time5 = :time5")})
public class MovieTimes implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MovieTimesPK movieTimesPK;
    @Size(max = 10)
    @Column(name = "TIME1")
    private String time1;
    @Size(max = 10)
    @Column(name = "TIME2")
    private String time2;
    @Size(max = 10)
    @Column(name = "TIME3")
    private String time3;
    @Size(max = 10)
    @Column(name = "TIME4")
    private String time4;
    @Size(max = 10)
    @Column(name = "TIME5")
    private String time5;
    @JoinColumn(name = "MOVIE_ID", referencedColumnName = "MOVIE_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Movie movie;
    @JoinColumn(name = "THEATER_ID", referencedColumnName = "THEATER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Theater theater;

    public MovieTimes() {
    }

    public MovieTimes(MovieTimesPK movieTimesPK) {
        this.movieTimesPK = movieTimesPK;
    }

    public MovieTimes(String theaterId, String movieId) {
        this.movieTimesPK = new MovieTimesPK(theaterId, movieId);
    }

    public MovieTimesPK getMovieTimesPK() {
        return movieTimesPK;
    }

    public void setMovieTimesPK(MovieTimesPK movieTimesPK) {
        this.movieTimesPK = movieTimesPK;
    }

    public String getTime1() {
        return time1;
    }

    public void setTime1(String time1) {
        this.time1 = time1;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }

    public String getTime3() {
        return time3;
    }

    public void setTime3(String time3) {
        this.time3 = time3;
    }

    public String getTime4() {
        return time4;
    }

    public void setTime4(String time4) {
        this.time4 = time4;
    }

    public String getTime5() {
        return time5;
    }

    public void setTime5(String time5) {
        this.time5 = time5;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Theater getTheater() {
        return theater;
    }

    public void setTheater(Theater theater) {
        this.theater = theater;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (movieTimesPK != null ? movieTimesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MovieTimes)) {
            return false;
        }
        MovieTimes other = (MovieTimes) object;
        if ((this.movieTimesPK == null && other.movieTimesPK != null) || (this.movieTimesPK != null && !this.movieTimesPK.equals(other.movieTimesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.MovieTimes[ movieTimesPK=" + movieTimesPK + " ]";
    }
    
}
