/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author carmen
 */
@Entity
@Table(name = "THEATER")
@NamedQueries({
    @NamedQuery(name = "Theater.findAll", query = "SELECT t FROM Theater t")
    , @NamedQuery(name = "Theater.findByTheaterId", query = "SELECT t FROM Theater t WHERE t.theaterId = :theaterId")
    , @NamedQuery(name = "Theater.findByName", query = "SELECT t FROM Theater t WHERE t.name = :name")
    , @NamedQuery(name = "Theater.findByStreet", query = "SELECT t FROM Theater t WHERE t.street = :street")
    , @NamedQuery(name = "Theater.findByCity", query = "SELECT t FROM Theater t WHERE t.city = :city")
    , @NamedQuery(name = "Theater.findByState", query = "SELECT t FROM Theater t WHERE t.state = :state")
    , @NamedQuery(name = "Theater.findByZip", query = "SELECT t FROM Theater t WHERE t.zip = :zip")
    , @NamedQuery(name = "Theater.findMovies", query = "SELECT m FROM Movie m, MovieTimes t WHERE m.movieId = t.movieTimesPK.movieId and t.movieTimesPK.theaterId = :theaterId")})
public class Theater implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "THEATER_ID")
    private String theaterId;
    @Size(max = 25)
    @Column(name = "NAME")
    private String name;
    @Size(max = 25)
    @Column(name = "STREET")
    private String street;
    @Size(max = 20)
    @Column(name = "CITY")
    private String city;
    @Size(max = 2)
    @Column(name = "STATE")
    private String state;
    @Column(name = "ZIP")
    private Integer zip;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "theater")
    private List<MovieTimes> movieTimesList;

    public Theater() {
    }

    public Theater(String theaterId) {
        this.theaterId = theaterId;
    }

    public String getTheaterId() {
        return theaterId;
    }

    public void setTheaterId(String theaterId) {
        this.theaterId = theaterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getZip() {
        return zip;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public List<MovieTimes> getMovieTimesList() {
        return movieTimesList;
    }

    public void setMovieTimesList(List<MovieTimes> movieTimesList) {
        this.movieTimesList = movieTimesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (theaterId != null ? theaterId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Theater)) {
            return false;
        }
        Theater other = (Theater) object;
        if ((this.theaterId == null && other.theaterId != null) || (this.theaterId != null && !this.theaterId.equals(other.theaterId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Theater[ theaterId=" + theaterId + " ]";
    }
    
}
